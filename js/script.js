function openMenu(btn) {
    if (btn === 'online') {
        $('#onlineMenu').addClass('show');
        $('#adminMenu').removeClass('show');
    } else {
        $('#adminMenu').addClass('show');
        $('#onlineMenu').removeClass('show');
    }
}

function changeButtonName(text, type) {
    if (type === 'admin') {
        $('#menuBtn2 span').text(text)
    } else {
        $('#menuBtn1 span').text(text)
    }
}

function closeMenu(type) {
    if (type === 'online') {
        $('#onlineMenu').removeClass('show');
    } else {
        $('#adminMenu').removeClass('show');
    }
}

var menu = $('.profileMenuDiv');
$(document).on('click', function (e) {
    if (menu.has(e.target).length === 0) {
        menu.removeClass('show');
        $('#onlineMenu').removeClass('show');
        $('#adminMenu').removeClass('show');
    }
});

function checkClass() {
    if ($('#sidebar').hasClass('active')) {
        $('.firstSubmenuItem').addClass('firstSubmenuItem');
    } else {
        $('.firstSubmenuItem').removeClass('firstSubmenuItem');
        $('.submenuList').removeClass('show');
    }
}

